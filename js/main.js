$(function() {
				var drawProgressBar = function(selector, skillValue, skillText){
					var bar = new ProgressBar.Circle(selector, {
					strokeWidth: 6,
					color: '#FFEA82',
					trailColor: '#eee',
					trailWidth: 1,
					easing: 'bounce',
					duration: 3000,
					svgStyle: null,
					text: {
						value: '',
						alignToBottom: false
					},
					from: {color: '#33ff00'},
					to: {color: '#ff9900'},
					// Set default step function for all animate calls
					step: (state, bar) => {
						bar.path.setAttribute('stroke', state.color);
						var value = Math.round(bar.value() * 100);
						if (value === 0) {
						bar.setText(skillText);
						} else {
						bar.setText(skillText);
						}

						bar.text.style.color = state.color;
					}
					});
					bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
					bar.text.style.fontSize = '1.2rem';
					bar.animate(skillValue);  // Number from 0.0 to 1.0
				}
				var Page = (function() {

					var $navArrows = $( '#nav-arrows' ),
						$nav = $( '#nav-dots > span' ),
						slitslider = $( '#slider' ).slitslider( {
							onBeforeChange : function( slide, pos ) {

								$nav.removeClass( 'nav-dot-current' );
								$nav.eq( pos ).addClass( 'nav-dot-current' );
								$('#skills > .three.column.row > .column').empty();
								$('.ui.grid.portfolio').empty();
								$('#welcomePage').empty();
                                $('#blog').empty();
																$('#malwaretoolz').empty();

							},
							onAfterChange : function(slide, pos){
									if(pos == 2){
										$('#col1').append('<div class="animated pulse" id="html5Skill"></div>');
										drawProgressBar(html5Skill, 0.8, 'HTML5');
										$('#col2').append('<div class="animated pulse" id="css3Skill"></div>');
										drawProgressBar(css3Skill, 0.6, 'CSS3');
										$('#col3').append('<div class="animated pulse" id="javascriptSkill"></div>');
										drawProgressBar(javascriptSkill, 0.4, 'JavaScript');
										$('#col4').append('<div class="animated pulse" id="csharpSkill"></div>');
										drawProgressBar(csharpSkill, 0.7, 'C#');
										$('#col5').append('<div class="animated pulse" id="sqlSkill"></div>');
										drawProgressBar(sqlSkill, 0.2, 'SQL');
										$('#col6').append('<div class="animated pulse" id="angularSkill"></div>');
										drawProgressBar(angularSkill, 0.4, 'AngularJS');
										$('#col7').append('<div class="animated pulse" id="jquerySkill"></div>');
										drawProgressBar(jquerySkill, 0.4, 'jQuery');
										$('#col8').append('<div class="animated pulse" id="semanticSkill"></div>');
										drawProgressBar(semanticSkill, 0.3, 'Semantic UI');
										$('#col9').append('<div class="animated pulse" id="bootstrapSkill"></div>');
										drawProgressBar(bootstrapSkill, 0.7, 'Bootstrap');

									}

									if(pos == 0){
										$('#welcomePage').append(' <div class="eight wide column"> <div class="animated zoomInDown main-title"> <h1 class="ui header" >Krzysztof Ciępka</h1> <h2 class="ui header">Full Stack Developer</h2> </div> </div> <div class="eight wide column"><div class="photo-album animated zoomIn"><a href="#" class="medium polaroid img9 hoverZoomLink"><img src="img/me.png" alt="">to ja :)</a> </div> </div> <div class="eight wide column"> </div> <div class="eight wide column"> <div class="animated zoomInUp main-summary"> <h3 class="ui header centered">o mnie</h3> <p> Od ponad 3 lat zawodowo zajmuję się rozwojem oprogramowania. Specjalizuję się w platformie .NET oraz frameworkach frontendowych. Posiadam również doświadczenie w embedded. Miałem okazję pracować w dużych projektach a także w startupach zarówno w Polsce jak i za granicą. W wolnych chwilach prowadzę bloga o programowaniu. </p> </div> </div>');
									}

									if(pos ==1){
										$('.ui.grid.portfolio').append('<div class="three column row"> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://januszex.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Bootstrap</span> </div> <img src="img/1.png" /> </a> </div> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://freelancertemplate.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Bootstrap</span> </div> <img src="img/2.png" /> </a> </div> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://dashtemplate.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Bootstrap</span> <span>AngularJS</span> <span>Chart.js</span> </div> <img src="img/3.png" /> </a> </div> </div> <div class="three column row"> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://multipainter.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Semantic UI</span> <span>AngularJS</span> <span>Paper.js</span> </div> <img src="img/4.png" /> </a> </div> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://multichatter.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Semantic UI</span> <span>AngularJS</span> <span>ASP.NET Core</span> <span>SignalR</span> </div> <img src="img/5.png" /> </a> </div> <div class="column"> <a class="animated zoomIn view" target="_blank" href="http://askthebot.cu.cc"> <div class="view-back"> <span>HTML5</span> <span>CSS3</span> <span>Semantic UI</span> <span>ASP.NET MVC</span> <span>SignalR</span> </div> <img src="img/6.png" /> </a> </div> </div>');


										Modernizr.load({
                                            test: Modernizr.csstransforms3d && Modernizr.csstransitions,
                                            yep : ['js/jquery.hoverfold.js'],
                                            nope: 'css/fallback.css',
                                            callback : function( url, result, key ) {

                                                if( url === 'js/jquery.hoverfold.js' ) {
                                            $( '.three.column.row .column' ).hoverfold();
                                                }

                                            }
                                        });
									}

                                    if(pos==3){
                                        $('#blog').append(' <div class="two column row blog-summary"> <div class="column"> <h1 class="ui header blog-title">Zajrzyj na bloga!</h1> <p>Zapraszam do odwiedzin mojego bloga poświęconego ogólnie pojętemu programowaniu, nie tylko związanemu z .NET</p> </div> <div class="column"> <a target="_blank" href="http://fatalexception.eu.org"><div class="blogbanner animated lightSpeedIn" ></div></a> </div> </div> <div class="one column centered row "> <div class="column"> <a target="_blank" href="http://fatalexception.eu.org"> <div class=" animated lightSpeedIn blog-button"> <img class="wraption" src="img/blog.jpg" title="fatalexception.eu.org" /> </div> </a> </div> </div>');

                                        $('img.wraption').wraption();
                                    }
																		if(pos==4){
																			$('#malwaretoolz').append('<div class="two column row malwaretoolz-summary animated rollIn"> <div class="column"> <h1 class="ui header malwaretoolz-title">Malware Toolz</h1> <p>Fanów wczesnego Internetu zapraszam do zerknięcia na klimatyczną stronę z narzędziami dla początkujących hakerów mojego autorstwa. ;)</p> </div> <div class="column"> <a target="_blank" href="https://malwaretoolz.eu.org"><img class="malwaretoolz-screen" src="img/malwaretoolz.png" /></a> </div> </div>')
																		}
							}
						} ),

						init = function() {
							$('.sl-slide').removeClass('hidden');
							$('#welcomePage').append(' <div class="eight wide column"> <div class="animated zoomInDown main-title"> <h1 class="ui header" >Krzysztof Ciępka</h1> <h2 class="ui header">Full Stack Developer</h2> </div> </div> <div class="eight wide column"><div class="photo-album animated zoomIn"><a href="#" class="medium polaroid img9 hoverZoomLink"><img src="img/me.png" alt="">to ja :)</a> </div></div> <div class="eight wide column"> </div> <div class="eight wide column"> <div class="animated zoomInUp main-summary"> <h3 class="ui header centered">o mnie</h3> <p> Od ponad 3 lat zawodowo zajmuję się rozwojem oprogramowania. Specjalizuję się w platformie .NET oraz frameworkach frontendowych. Posiadam również doświadczenie w embedded. Miałem okazję pracować w dużych projektach a także w startupach zarówno w Polsce jak i za granicą. W wolnych chwilach prowadzę bloga o programowaniu. </p> </div> </div>');
							initEvents();


						},
						initEvents = function() {

							// add navigation events
							$navArrows.children( ':last' ).on( 'click', function() {
								slitslider.next();
								return false;

							} );

							$navArrows.children( ':first' ).on( 'click', function() {
								slitslider.previous();
								return false;

							} );

							$nav.each( function( i ) {

								$( this ).on( 'click', function( event ) {

									var $dot = $( this );

									if( !slitslider.isActive() ) {

										$nav.removeClass( 'nav-dot-current' );
										$dot.addClass( 'nav-dot-current' );

									}

									slitslider.jump( i + 1 );
									return false;

								} );

							} );

						};

						return { init : init };

				})();

				Page.init();

			});
